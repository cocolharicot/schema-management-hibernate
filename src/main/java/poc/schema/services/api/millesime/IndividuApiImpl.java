package poc.schema.services.api.millesime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import poc.schema.model.millesime.Individu;
import poc.schema.services.service.millesime.IndividuService;

import java.util.List;
@Service
public class IndividuApiImpl implements IndividuApi{
    @Autowired
    private IndividuService individuService;
    @Override
    public List<Individu> getAllIndividus() {
        return individuService.findAll();
    }

    @Autowired
    public IndividuService getIndividuService() {
        return individuService;
    }

    @Override
    public Individu getIndividu(long id) {
        return individuService.getIndividu(id);
    }
}
