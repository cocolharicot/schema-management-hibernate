package poc.schema.services.api.millesime;

import org.springframework.stereotype.Service;
import poc.schema.model.millesime.Individu;

import java.util.List;

@Service
public interface IndividuApi {
    List<Individu> getAllIndividus();

    Individu getIndividu(long id);
}
