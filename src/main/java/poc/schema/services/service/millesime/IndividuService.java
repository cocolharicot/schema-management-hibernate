package poc.schema.services.service.millesime;

import org.springframework.stereotype.Service;
import poc.schema.model.millesime.Individu;

import java.util.List;

@Service
public interface IndividuService {
    List<Individu> findAll();

    Individu getIndividu(long id);
}
