package poc.schema.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import poc.schema.dao.configuration.TenantConnectionProvider;
import poc.schema.model.millesime.Individu;
import poc.schema.services.api.millesime.IndividuApi;

import java.util.List;

@RestController
@RequestMapping("/individu")
public class IndividuController {
    private static Logger logger = LoggerFactory.getLogger(IndividuController.class);
    @Autowired
    private IndividuApi individuApi;

    @GetMapping("/")
    public List<Individu> getIndividus(){
        return individuApi.getAllIndividus();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Individu> getIndividu(@PathVariable long id){
        logger.info("Recuperation de l'individu");
        Individu individu=individuApi.getIndividu(id);
        logger.info("Individu {}",individu.getName());
        return new ResponseEntity<>(individu, HttpStatus.OK);
    }

    @GetMapping("/get-metadata")
    public HttpStatus getSchemaInfo(){
        return HttpStatus.ACCEPTED;
    }
}
    